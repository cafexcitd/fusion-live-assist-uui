<?php

// configure the JSON to use in the session.
$json = '
    {
        "webAppId": "my-web-app-id123",
        "allowedOrigins": ["*"],
        "urlSchemeDetails": {
            "host": "rp.example.com",
            "port": "8080",
            "secure": false
        },
        "uuiData": "Variable1",
        "additionalAttributes":
        {
            "AED2.metadata":
            {
                "role": "consumer"
            }
        }
    }
';

// configure the curl options
$ch = curl_init("http://gateway.example.com:8080/gateway/sessions/session");
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
curl_setopt($ch, CURLOPT_HTTPHEADER, array(         
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json))                                
);

// execute HTTP POST & close the connection
$response = curl_exec($ch);
curl_close($ch);

// add CORS header
header("Access-Control-Allow-Origin: *");

// decode the JSON and pick out the ID
$decodedJson = json_decode($response);
$id = $decodedJson->{'sessionid'};

// echo the ID we've retrieved
echo $id;

?>